package com.gitee.sosuyoung.autoconfigure.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * @author wxm
 * @version 1.0
 * @since 2021/6/16 17:29
 */
@Data
@Configuration
@ConfigurationProperties(prefix = "spring.influx")
public class InfluxdbProperties {
    /**
     * 用户名
     */
    private String user;
    /**
     * 密码
     */
    private String password;
    /**
     * 连接地址
     */
    private String url;
    /**
     * 数据库名
     */
    private String database;
}
