package com.gitee.sosuyoung.autoconfigure;

import com.gitee.sosuyoung.core.InfluxdbTemplate;
import org.influxdb.InfluxDB;
import org.influxdb.InfluxDBFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import com.gitee.sosuyoung.autoconfigure.properties.InfluxdbProperties;


/**
 * @author wxm
 * @version 1.0
 * @since 2021/6/16 13:39
 */
@Configuration
@EnableConfigurationProperties({InfluxdbProperties.class})
@ConditionalOnProperty(prefix = "spring.influx", value = "enable", matchIfMissing = true)
public class InfluxdbAutoConfiguration {

    public InfluxdbAutoConfiguration() {

    }


    @Bean
//    @ConditionalOnMissingBean
    @Primary
    public InfluxDB influxdb(InfluxdbProperties influxdbProperties) {
        InfluxDB influxDB = InfluxDBFactory.connect(influxdbProperties.getUrl(), influxdbProperties.getUser(), influxdbProperties.getPassword());
        influxDB.setDatabase(influxdbProperties.getDatabase());
        influxDB.setLogLevel(InfluxDB.LogLevel.BASIC);
        return influxDB;
    }


    @Bean
    public InfluxdbTemplate influxdbTemplate(InfluxdbProperties influxdbProperties) {
        return new InfluxdbTemplate(influxdbProperties);
    }


}
